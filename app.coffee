config = require './config'
express = require 'express'
bodyParser = require 'body-parser'
mongoose = require 'mongoose'
morgan = require 'morgan'
path = require 'path'
jwtDecoder = require 'jsonwebtoken'
bcrypt = require 'bcrypt-nodejs'
http = require 'http'
unirest = require 'unirest'
cheerio = require 'cheerio'
jwt = require 'express-jwt'
jwtPayloadDecoder = require 'jwt-payload-decoder'

jwtCheck = jwt({
  secret: new Buffer('pp4YCvybflbm5te2zsGfAdfmzT1gvE5HgkDWmKwSJyX6nN6-C2gJP-BILLYF-9Ad', 'base64'),
  audience: 'FC61uqavSQvYt0pFb94YRSwYdWdp28XV'
});

#Cria a aplicação Express e uma sub-aplicação responsável pela parte da API
mainApp = express()
api = express()
server = http.Server mainApp
io = require("socket.io").listen(server)
#Atribui a sub-aplicação ao endereço '/api'

#mainApp.use '/api', jwtCheck, (req, res, next) ->
#	token = req.headers.authorization.split(' ')[1]
#	payload = jwtDecoder.decode(token)
#	req.userId = payload.sub
#	next()

mainApp.use '/api', api

#Configura a sub-aplicação para trabalhar com JSON
api.use bodyParser.urlencoded(extended: true)
api.use bodyParser.json()

#Configura cabeçalhos padrão para todas as respostas HTTP fornecidas pela aplicação
api.use (req, res, next) ->
	res.setHeader 'Access-Control-Allow-Origin', 'http://localhost:8100'
	res.setHeader 'Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE'
	res.setHeader 'Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization'
	next()

mainApp.use (req, res, next) ->
	res.setHeader 'Access-Control-Allow-Origin', 'http://localhost:8100'
	res.setHeader 'Access-Control-Allow-Credentials', true
	res.setHeader 'Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE'
	res.setHeader 'Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization'
	next()
#Inicializa a biblioteca de log para o ambiente de desenvolvimento
mainApp.use morgan 'dev'

#Realiza a conexão com o Banco de Dados de acordo com a string definida no arquivo config.coffee
mongoose.connect config.database

#Obtém modelos
Log = require('./app/models/log')(mongoose)
User = require('./app/models/user')(mongoose)
Job = require('./app/models/job')(mongoose)
Cnae = require('./app/models/cnae')(mongoose)
Chat = require('./app/models/chat')(mongoose)

#Obtém serviços
authService = require('./app/services/auth-service')(User, jwt, config)
logService = require('./app/services/log-service')(Log, config)
cnaeService = require('./app/services/cnae-service')(Cnae, config)
jobService = require('./app/services/job-service')(Job, User, config)
meiService = require('./app/services/mei-service')(cheerio, unirest, cnaeService, User)
dashboardService = require('./app/services/dashboard-service')(User, Job, Cnae)

#Inicializa rotas
baseREST = require('./app/routes/base-rest-routes')(express)

dashboardRoutes = require('./app/routes/dashboard-routes')(express, dashboardService)
api.use '/dashboard', dashboardRoutes

meiRoutes = require('./app/routes/mei-routes')(express, meiService)
api.use '/mei', meiRoutes

logRoutes = require('./app/routes/log-routes')(express, logService)
api.use '/', logRoutes

nearbyRoutes = require('./app/routes/nearby-routes')(express, jobService)
api.use '/nearby', nearbyRoutes

jobSearchRoutes = require('./app/routes/job-search-routes')(express, jobService)
api.use '/job-search', jobSearchRoutes

#authRoutes = require('./app/routes/auth-routes')(express, authService)
#api.use '/', authRoutes

userRoutes = require('./app/routes/user-routes')(baseREST, User)
api.use '/', userRoutes

jobRoutes = require('./app/routes/job-routes')(baseREST, Job)
api.use '/', jobRoutes

cnaeRoutes = require('./app/routes/cnae-routes')(baseREST, Cnae)
api.use '/', cnaeRoutes

chatRoutes = require('./app/routes/chat-routes')(express, Chat, io)
api.use '/chat', chatRoutes

bootstrap = require('./bootstrap')(User, Cnae, Job)
#bootstrap.run()

#Inicia o servidor na porta informada pela linha de comando ou na padrão definida no arquivo config.coffee
server.listen config.port
console.log 'Aplicação rodando na porta: ' + config.port
