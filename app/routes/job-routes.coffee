module.exports = (RESTRoute, Job) ->
  class JobRoutes extends RESTRoute

    constructor: ->
      @createFields = ['title', 'description', 'cnae_code', 'cnae_name', 'location', 'date', 'creator_id', 'creator_name', 'creator_photo']
      @updateFields = ['title', 'description', 'cnae_code', 'cnae_name', 'location', 'rating']
      super(Job, 'jobs')


  return new JobRoutes().getRoute()
