module.exports = (RESTRoute, User) ->
  class UserRoutes extends RESTRoute
    constructor: ->
      @createFields = [
        'name',
        'email',
        'photo_url',
        'facebook_id',
        'isMei',
        'birthday',
        'competences',
        'accessToken',
        'location',
        'bio'
      ]

      @updateFields = [
        'name',
        'email',
        'photo_url',
        'facebook_id',
        'isMei',
        'birthday',
        'accessToken',
        'location',
        'bio'
      ]
      super(User, 'users')

    createModel: (model, callback)->
      update = @updateModel
      fields = @updateFields
      User.getByFacebookID model.facebook_id, (user) ->
        if(user)
          console.log 'Atualizando Usuário'
          console.log model
          user.last_online = new Date()
          user[field] = model[field] for field in fields when model[field] isnt undefined
          update user, callback
        else
          super model, callback

  return new UserRoutes().getRoute()
