module.exports = (User, Job, Cnae) ->
	DashboardService =
		getInfo: (callback)->
			info = {}
			User.GetMeiCount (mei_count) ->
				User.GetNonMeiCount (non_mei_count) ->
					Job.GetFinalizedCount (finalized_job_count) ->
						Job.GetAvaliableCount (available_job_count) ->
							Cnae.CountAll (cnae_count) ->
								info.mei_count = mei_count
								info.non_mei_count = non_mei_count
								info.available_job_count = available_job_count
								info.finalized_job_count = finalized_job_count
								info.cnae_count = cnae_count
								callback info

	return DashboardService
