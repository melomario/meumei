module.exports = (Cnae, config) ->
  class CnaeService
    addNew: (cnaeList) ->
      for current_cnae in cnaeList
        do(current_cnae) ->
          Cnae.getByCode current_cnae.codigo, (cnae)->
            console.log "Analisando CNAE " + current_cnae.codigo
            if(cnae)
              console.log "CNAE existente: " + current_cnae.atividade
            else
              console.log "Inserindo Cnae"
              new_cnae = new Cnae()
              new_cnae.code = current_cnae.codigo
              new_cnae.name = current_cnae.atividade
              new_cnae.save (error) ->
                console.log "Erro ao inserir dados: " + error if error
              console.log "Cnae inserido com sucesso"

  return new CnaeService()
