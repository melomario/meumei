module.exports = (cheerio, unirest, cnaeService, User)->
  INDEX_CODIGO = 1
  INDEX_ATIVIDADE = 2
  generateSession = () ->
    session = 'JSESSIONID='
    session += '0123456789ABCDEF'.charAt( Math.random() * 100 % 16 ) while session.length < 32
    session += '.node5; _skinNameCookie=mei'
    session

  extractCompetences = (html, user)->
    data = {}
    data.success = false
    data.atividades = []
    atividades = []
    codigos = []
    $ = cheerio.load html
    $('.outputLabelCertificado').each (labelCounter, labelElement) ->
      if($(labelElement).contents().text().indexOf('Situação Cadastral Vigente') in [0..5])
        #Descobrimos se está ativo:
        $(labelElement).parentsUntil('tr').siblings().children('td').children('span').each (counter, element) ->
          data.ativo = $(element).contents().text() == 'ATIVO'
          data.success = true

      if($(labelElement).contents().text().indexOf('Código da Atividade Principal') in [0..5])
        $(labelElement).parentsUntil('tr').siblings().children('td').children('span').each (counter, element) ->
          codigos.push $(element).contents().text()

      if($(labelElement).contents().text().indexOf('Descrição da Atividade Principal') in [0..5])
        $(labelElement).parentsUntil('tr').siblings().children('td').children('span').each (counter, element) ->
          atividades.push $(element).contents().text()


    $('table[title="Cnae Secundária"]').children('tbody').children('tr').each (counter, element) ->
      $(element).children('td').each (i, coluna) ->
        codigos.push $(coluna).contents().text() if i == INDEX_CODIGO
        atividades.push $(coluna).contents().text() if i == INDEX_ATIVIDADE

    for atividade, index in atividades
      data.atividades.push {codigo: codigos[index], atividade: atividade}

    cnaeService.addNew data.atividades

    User.findById user, (error, result) ->
      result.competences = data.atividades
      result.isValidated = true
      result.save (error) ->
        throw error if error
    return data


  MeiService =
    extractCompetences: (html, user) ->
      extractCompetences html, user

    getCaptcha: (callback) ->
      data = {session: generateSession()}

      unirest.get 'http://www22.receita.fazenda.gov.br/inscricaomei/private/pages/certificado_acesso.jsf'
      .header 'Cookie', data.session
      .end (html)->
        $ = cheerio.load html.body
        data.faces_id = $("input[name='javax.faces.ViewState']").attr('value')

        unirest.post('http://captcha2.servicoscorporativos.serpro.gov.br/captcha/1.0.0/imagem')
        .header 'Content-Type', 'text/plain;charset=UTF-8'
        .send 'd394cd471c474b30bf6e9855bf7a5fc5'
        .end (captcha) ->
            data.id = captcha?.body?.split("@")[0]
            data.image = captcha?.body?.split("@")[1]
            callback data

    validateMei: (params, callback) ->
      console.log "User: " + params.user
      unirest.post 'http://www22.receita.fazenda.gov.br/inscricaomei/private/pages/certificado_acesso.jsf'
      .header 'Cookie', params.sessionId
      .header 'Content-Type', 'application/x-www-form-urlencoded'
      .form
        'meiMB_cpf': params.cpf
        'meiMB_dataNascimento': params.birthday
        'txtToken_captcha_serpro_gov_br': params.captchaToken
        'txtTexto_captcha_serpro_gov_br': params.captcha
        'form:j_id30': 'Prosseguir'
        'form': 'form'
        'autoScroll': ''
        'javax.faces.ViewState': params.faces_id
      .end (html) ->
        callback extractCompetences(html.body, params.user)

  return MeiService
