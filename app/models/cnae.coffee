module.exports = (mongoose) ->
  Schema = mongoose.Schema
  CnaeSchema = new Schema(
    code: String
    name: String
  )

  CnaeSchema.statics.getByCode = (code, callback) ->
    this.findOne( code: code ).exec (error, cnae) ->
      throw error if error
      callback cnae

  CnaeSchema.statics.CountAll = (callback) ->
    this.count().exec (error, count) ->
      console.log count
      throw error if error
      callback count

  return mongoose.model 'Cnae', CnaeSchema
