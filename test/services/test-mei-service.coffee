fs = require 'fs'
cheerio = require 'cheerio'
cnaeService =
  addNew: (cnaeList) ->
     
meiService = require('../../app/services/mei-service')(cheerio, undefined, cnaeService)

describe 'Serviço de Integração com o portal MEI', ->
  describe 'A importação do arquivo HTML', ->
    it 'deve retornar um JSON com uma lista de CNAEs do MEI consultado', (done)->
      fs.readFile 'test//test-data/mei.html', (error, data) ->
        meiInfo = meiService.extractCompetences data.toString()
        meiInfo.atividades.should.have.lengthOf 13
        meiInfo.ativo.should.be.true
        done()

    it 'deve retornar o status de erro caso não consiga validar o MEI'
